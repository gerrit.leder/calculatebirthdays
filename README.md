Brought to you by Gerrit Leder (gerrit.leder@gmail.com).

Java implementation with lambdas of the xing style "show next birthdays" functionality.
And a birthday paradoxon example, cp. https://en.wikipedia.org/wiki/Birthday_problem.

Please use OpenJDK-17-JDK.

Project repository: https://gitlab.com/gerrit.leder/calculatebirthdays

Project homepage: https://burzltag.org
