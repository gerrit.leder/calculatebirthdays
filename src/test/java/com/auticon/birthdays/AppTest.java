package com.auticon.birthdays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//import static org.hamcrest.Matchers.greaterThan;
//import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class AppTest {

    App app;

    @BeforeEach
    void setUp() {
        app = new App();
    }

    @Test
    void testProbabilityADashOneContact() {
        List<Person> one_contact = new ArrayList<>();

        double result;

        int size;

        Person test = new Person("test", LocalDate.of(2000, Month.FEBRUARY, 29), Person.Sex.FEMALE, "");

        one_contact.add(test);


        app.setContacts(one_contact);


        size = app.getContacts().size();

        //    System.out.println("size: "+ size);

        result = app.probability_a_dash(size);

        //  System.out.println("result: "+ result);

        assertThat( result , equalTo(1.0d));
    }


    @Test
    void testProbabilityADash23Contact() {
        List<Person> one_contact = new ArrayList<>();

        double result;


        result = app.probability_a_dash(23);

        //System.out.println("result: "+ result);

	assertThat(result, lessThan(0.5d));
	assertThat(result, greaterThan(0.49d));

    }

   @Test
    void testProbabilityADash70Contact() {
        List<Person> one_contact = new ArrayList<>();

        double result;


        result = app.probability_a_dash(70);

 //       System.out.println("result: "+ result);

	assertThat(result, lessThan(0.001d));
	assertThat(result, greaterThan(0.0008d));

    }

}
