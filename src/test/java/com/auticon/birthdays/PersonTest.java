package com.auticon.birthdays;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
// import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

public class PersonTest {

    static App app;

    static Person eva =
            new Person("Eva", LocalDate.of(2010, Month.SEPTEMBER, 24), Person.Sex.FEMALE, "");

    static Person pauline = new Person("Pauline", LocalDate.of(2012, Month.FEBRUARY, 13), Person.Sex.FEMALE, "");

    static Person test = new Person("test", LocalDate.of(2000, Month.FEBRUARY, 29), Person.Sex.FEMALE, "");

    @BeforeAll
    public static void setUp() throws Exception {

        app = new App();
        SerializeToXML.contacts.add(eva);

        SerializeToXML.contacts.add(pauline);

        SerializeToXML.contacts.add(test);

    }

    @AfterAll
    public static void tearDown() throws Exception {

        app = null;

    }



    @Test
    public void getAge() {

        assertEquals(eva.getAge(LocalDate.of(2010, Month.SEPTEMBER, 24)), 0);

    }

    @Test
    public void getGender() {

        assertEquals(pauline.getGender(), Person.Sex.FEMALE);
    }

    @Test
    public void monthsToNextBirthdaySameLocalDate() {

        assertEquals(pauline.monthsToNextBirthday(LocalDate.of(2012, Month.FEBRUARY, 13), LocalDate.of(2012, Month.FEBRUARY, 13)), 0);

    }

    @Test
    public void monthsToNextBirthdayPlusMonthsOne() {

        assertEquals(pauline.monthsToNextBirthday(LocalDate.of(2012, Month.FEBRUARY, 13), LocalDate.of(2012, Month.MARCH, 13)),11);

    }

    @Test
    public void daysToNextBirthday() {


            assertEquals(pauline.daysToNextBirthday(LocalDate.of(2012, Month.FEBRUARY, 13), LocalDate.of(2012, Month.FEBRUARY, 13)), 0);


    }

    @Test
    public void daysToNextBirthdayPlusDaysTwo() {


        assertEquals(pauline.daysToNextBirthday(LocalDate.of(2012, Month.FEBRUARY, 13), LocalDate.of(2012, Month.FEBRUARY, 15)), 364);


    }

    @Test
    public void daysToNextBirthdayMinusDaysTwo() {


        assertEquals(pauline.daysToNextBirthday(LocalDate.of(2012, Month.FEBRUARY, 13), LocalDate.of(2012, Month.FEBRUARY, 11)), 2);


    }

    @Test
    public void daysToNextBirthdayLeapLeap() {


        assertEquals(test.daysToNextBirthday(LocalDate.of(2000, Month.FEBRUARY, 29), LocalDate.of(2004, Month.FEBRUARY, 29)), 0);


    }

    @Test
    public void daysToNextBirthdayLeapNormal() {


        assertEquals(test.daysToNextBirthday(LocalDate.of(2000, Month.FEBRUARY, 29), LocalDate.of(2001, Month.MARCH, 1)), 0);


    }


    @Test
    public void daysToNextBirthdayBeforeLeap() {


        assertEquals(test.daysToNextBirthday(LocalDate.of(2000, Month.FEBRUARY, 29), LocalDate.of(2003, Month.MARCH, 1)), 0);


    }

    @Test
    public void daysToNextBirthdayBeforeNormal() {


        assertEquals(test.daysToNextBirthday(LocalDate.of(2000, Month.FEBRUARY, 29), LocalDate.of(2005, Month.MARCH, 1)), 0);


    }


    @Test
    public void evaAndPaulineSameDateFails() {
        boolean result;

        result = app.sameDayAndMonth(eva.getBirthday(),pauline.getBirthday());

        assertFalse(result);


    }

    @Test
    public void newYearSameDatePasses() {
        boolean result;

        LocalDate localDate = LocalDate.of(1999, Month.JANUARY, 1);

        result = app.sameDayAndMonth(localDate, localDate);

        assertTrue(result);


    }

    @Test
    public void differentNewYearSameDatePasses() {
        boolean result;

        LocalDate localDate_1 = LocalDate.of(1999, Month.JANUARY, 1);
        LocalDate localDate_2 = LocalDate.of(2001, Month.JANUARY, 1);

        result = app.sameDayAndMonth(localDate_1 , localDate_2);

        assertTrue(result);


    }

    @Test
    public void differentDatesLeapYearSameDateFails() {
        boolean result;

        LocalDate localDate_1 = LocalDate.of(2004, Month.FEBRUARY, 29);
        LocalDate localDate_2 = LocalDate.of(2008, Month.MARCH, 1);

        result = app.sameDayAndMonth(localDate_1 , localDate_2);

        assertFalse(result);


    }

}