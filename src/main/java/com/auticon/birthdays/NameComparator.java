package com.auticon.birthdays;

import java.util.*;

// creates the comparator for comparing name
class NameComparator implements Comparator<Person> {
  
    // override the compare() method
    public int compare(Person p1, Person p2)
    {
        return p1.name.compareTo(p2.name);
    }
}
