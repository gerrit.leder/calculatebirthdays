package com.auticon.birthdays;

import java.time.DateTimeException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.Date;

public class Person {

    public enum Sex {
        MALE, FEMALE, DIVERSE
    }

    public String name;
    private LocalDate birthday;
    private Sex gender;
    private String emailAddress;
    private boolean optInReceiveEmails;
    private boolean optOutReceiveEmails;

    private int age;

    public Person() {}

    public Person (String name, LocalDate birthday, Sex gender, String emailAddress) {
        this.name=name;
        this.birthday=birthday;
        this.gender=gender;
        this.emailAddress=emailAddress;
        this.optInReceiveEmails = false;
        this.optOutReceiveEmails = false;

    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public void setGender(Sex gender) {
        this.gender = gender;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setOptInReceiveEmails(boolean optInReceiveEmails) {
        this.optInReceiveEmails = optInReceiveEmails;
    }

    public void setOptOutReceiveEmails(boolean optOutReceiveEmails) {
        this.optOutReceiveEmails = optOutReceiveEmails;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge(LocalDate today) {
        return Period.between(this.birthday, today).getYears();
    }

    /*public int getAge() {
        return Period.between(this.birthday, LocalDate.now()).getYears();
    }*/


    public Sex getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public boolean getOptInReceiveEmails() {
        return optInReceiveEmails;
    }

    public boolean getOptOutReceiveEmails() {
        return optOutReceiveEmails;
    }

    public int getAge() {
        return age;
    }

    public void printPerson() {

        DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        int days = daysToNextBirthday(this.birthday, LocalDate.now());
        String format = "%-22s%-10s%-10s%s%n";

        if (days==0){
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")", "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": HAPPY BIRTHDAY!!!");

        } else if (days==1) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is one" + " day");
        } else if (days==2) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is two" + " days");
        } else if (days==3) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is three" + " days");
        }else if (days==4) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is four" + " days");
        }else if (days==5) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is five" + " days");
        }else if (days==6) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is six" + " days");
        }else if (days==7) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is seven" + " days");
        }else if (days==8) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is eight" + " days");
        }else if (days==9) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is nine" + " days");
        }else if (days==10) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is ten" + " days");
        }else if (days==11) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is eleven" + " days");
        }else if (days==12) {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is twelve" + " days");
        } else {
            System.out.printf(format, name + " (" + getAge( LocalDate.now()) + ")",  "|" + dtf.format(birthday), "|" + gender.toString(), "|" + emailAddress + ": time until next birthday is " + days + " days");
        }
    }
    int monthsToNextBirthday(LocalDate birthday, LocalDate today) {

        DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        LocalDate nextBirthday = LocalDate.of(today.getYear(), birthday.getMonth(), birthday.getDayOfMonth());

        if (nextBirthday.isBefore(today)) {

            nextBirthday = nextBirthday.plusYears(1);

        }

        Period period = Period.between(  today, nextBirthday);



        int mm = period.getMonths();



        return mm;
    }

    int daysToNextBirthday (LocalDate birthday, LocalDate today) {

       // DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

        try {

            LocalDate nextBirthday = LocalDate.of(today.getYear(), birthday.getMonth(), birthday.getDayOfMonth());
            return calculateDays(nextBirthday, today);

        } catch (DateTimeException ex)
        {

            LocalDate nextBirthday = LocalDate.of(today.getYear(), birthday.getMonth().plus(1), 1);
            return calculateDays(nextBirthday, today);

        }



    }

    private int calculateDays(LocalDate nextBirthday, LocalDate today) {
        if (nextBirthday.isBefore(today)) {

            nextBirthday = nextBirthday.plusYears(1);

        }

        long duration = ChronoUnit.DAYS.between( today, nextBirthday );



        int dd = (int) duration;



        return dd;

    }





}
