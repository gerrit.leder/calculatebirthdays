package com.auticon.birthdays;

import java.beans.Encoder;
import java.beans.Expression;
import java.beans.PersistenceDelegate;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class SerializeToXML {

    private static final String SERIALIZED_FILE_NAME="contacts.xml";

    private static final String SERIALIZED_FILE_NAME_1="contacts (1).xml";

    public static List<Person> contacts=new ArrayList();

    public static Contacts gerritsContacts=new Contacts();

    public static void main(String args[]){

    

        gerritsContacts.setContacts(contacts);

        writeContacts(gerritsContacts.getContacts());

    }

    public static void writeContacts(List<Person> contacts) {

        gerritsContacts.setContacts(contacts);

        XMLEncoder encoder=null;
        try{
            encoder=new XMLEncoder(new BufferedOutputStream(new FileOutputStream(SERIALIZED_FILE_NAME)));

            System.out.println("Success: Filename " + SERIALIZED_FILE_NAME + " written");

        }catch(FileNotFoundException fileNotFound){
            System.out.println("ERROR: While Creating or Opening the File " + SERIALIZED_FILE_NAME);




        }

        encoder.setPersistenceDelegate(LocalDate.class,
                new PersistenceDelegate() {
                    @Override
                    protected Expression instantiate(Object localDate, Encoder encdr) {
                        return new Expression(localDate,
                                LocalDate.class,
                                "parse",
                                new Object[]{localDate.toString()});
                    }
                });

        encoder.writeObject(gerritsContacts);
        encoder.close();
    }

}
