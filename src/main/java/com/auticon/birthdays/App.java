package com.auticon.birthdays;


import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Predicate;

public class App {

   // public static SerializeToXML serializeToXML = new SerializeToXML();

  //  public static DeserializeFromXML deserializeFromXML = new DeserializeFromXML();

    public static List<Person> getContacts() {
        return contacts;
    }

    public static void setContacts(List<Person> contacts) {
        App.contacts = contacts;
    }

    public static List<Person> contacts;

    public static void printPersonsWithPredicate(
            List<Person> roster, Predicate<Person> tester) {
        for (Person p : roster) {
            if (tester.test(p)) {
                p.printPerson();
            }
        }
    }

    public static void compareDayMonthSameDateAndPrint (List<Person> roster) {
        boolean result = false;
	boolean current_col = false;
        int nr = 1;
	List<Person> collision = new ArrayList<Person> ();
	//TreeSet<String> printed = new TreeSet<>();
	ArrayList<Person> printed = new ArrayList<Person> ();
	ArrayList<Person> tmp = new ArrayList<Person> ();
	ArrayList<Person> tmp2 = new ArrayList<Person> ();


        System.out.println("Collision groups:");

        for (int i=0; i < roster.size()-1; i++) {

	    collision.add (roster.get(i));

            for (int j=i+1; j < roster.size(); j++) {

                if (sameDayAndMonth(roster.get(i).getBirthday(), roster.get(j).getBirthday())) {

		    //debug:
                    //System.out.println(nr++ + ")");
                    //roster.get(i).printPerson();
                    //roster.get(j).printPerson();
		    //

                    //result = true;
		    current_col = true;
		   
		    collision.add (roster.get(j));
                 };
	   

	    

            }

	    if (current_col == true) {
		
		System.out.println();

		tmp = (ArrayList<Person>)printed.clone();
		tmp2 = (ArrayList<Person>)tmp.clone();

		tmp2.removeAll(collision);
		
		Collections.sort(tmp, new NameComparator());
		Collections.sort(tmp2, new NameComparator());
		Collections.sort(collision, new NameComparator());


		if (tmp.isEmpty()||tmp.equals(tmp2)) {
		System.out.println("Name (Age)            |Birthday |Gender   |eMail");
		System.out.println("----------------------|---------|---------|-----");
		}

		for (Person p:  collision) {
			if (!printed.contains(p)) {		
				p.printPerson();	
				printed.add(p);	
			}
		}

		if (tmp.isEmpty()||tmp.equals(tmp2)) {
			System.out.println("----------------------|---------|---------|-----");
		}
  	    	
	    }



	    current_col = false;
	    collision = new ArrayList<Person>();
	    //printed = new ArrayList<Person>();
	    tmp = new ArrayList<Person>();
	    tmp2 = new ArrayList<Person>();
        }

        System.out.println();

        //if (result) return true;

        //return false;



    }

    public static void sort(List<Person> roster) {

        Comparator<Person> byDaysToNextBirthday =  (p1, p2) -> ((Integer)(p1.daysToNextBirthday(p1.getBirthday(), LocalDate.now()))).compareTo(p2.daysToNextBirthday(p2.getBirthday(), LocalDate.now()));

        //Comparator<Person> bySex = (p1, p2) -> p1.getGender().compareTo(p2.getGender());

        roster.stream().sorted(byDaysToNextBirthday ).forEach(p -> p.printPerson());
    }

    public static boolean sameDayAndMonth (LocalDate birthday_1, LocalDate birthday_2) throws RuntimeException {

        if (birthday_1.getMonth()==birthday_2.getMonth() && birthday_1.getDayOfMonth()==birthday_2.getDayOfMonth())
        {
            return true;

        }
        else return false;

        //throw new RuntimeException("implement TC first");

    }

    public static void main(String[] args) {

        //serializeToXML.main(new String[0]);




        if (args.length!=0) {



            switch (args[0]) {
                case "add":

                    try {
                        readFileAdd();
                    } catch (FileNotFoundException e) {

                        addPersonToNewList(args);
                        break;
                    }

                    addPersonToContacts(args);
                    break;

                case "delete":

                    try {
                        readFilePrintDelete();
                        removePersonsFromContacts(args);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                    break;

                case "print":

                    try {
                        readFilePrintDelete();
                        printBirthdays(contacts);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                    break;
                case "opt-in":

                    optInEmailAddress(args);

                    break;
                case "opt-out":

                    optOutEmailAddress(args);

                    break;
                default: printHelp();
            }

        } else
            printHelp();


    }

    private static void optOutEmailAddress(String[] args) {
        TreeSet<String> emailAddresses = new TreeSet<>(Arrays.asList(Arrays.copyOfRange(args, 2, args.length)));
        emailAddresses.remove("");
        if (emailAddresses.isEmpty())
            System.out.println ("Please enter email addresses to opt out.");
        else {
            try {
                readFile();
                for (final String emailAddress : emailAddresses)
                {
                    int numberOfNewOptOuts = 0;
                    int numberOfPeopleWhoAlreadyOptedOut = 0;
                    for (Person person : contacts)
                    {
                        if (person.getEmailAddress().equalsIgnoreCase(emailAddress)) {
                            if (!person.getOptOutReceiveEmails()) {
                                person.setOptOutReceiveEmails(true);
                                person.setOptInReceiveEmails(false);
                                numberOfNewOptOuts++;
                            } else
                                numberOfPeopleWhoAlreadyOptedOut++;
                        }
                    }
                    if (numberOfNewOptOuts > 0)
                        System.out.format ("\"%s\": Great! We raised opt-outs for this email address from %d to %d.%n", emailAddress, numberOfPeopleWhoAlreadyOptedOut, numberOfPeopleWhoAlreadyOptedOut + numberOfNewOptOuts);
                    else if (numberOfPeopleWhoAlreadyOptedOut > 0)
                        System.out.format ("\"%s\": All %d people with this email address already opted out of receiving emails.%n", emailAddress, numberOfPeopleWhoAlreadyOptedOut);
                    else
                        System.out.format ("\"%s\": Nobody has this email address.%n", emailAddress);
                }
                SerializeToXML.writeContacts(contacts);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private static void optInEmailAddress(String[] args) {
        TreeSet<String> emailAddresses = new TreeSet<>(Arrays.asList(Arrays.copyOfRange(args, 2, args.length)));
        emailAddresses.remove("");
        if (emailAddresses.isEmpty())
            System.out.println ("Please enter email addresses to opt in.");
        else {
            try {
                readFile();
                for (final String emailAddress : emailAddresses)
                {
                    int numberOfNewOptIns = 0;
                    int numberOfPeopleWhoAlreadyOptedIn = 0;
                    for (Person person : contacts)
                    {
                        if (person.getEmailAddress().equalsIgnoreCase(emailAddress)) {
                            if (!person.getOptInReceiveEmails()) {
                                person.setOptInReceiveEmails(true);
                                person.setOptOutReceiveEmails(false);
                                numberOfNewOptIns++;
                            } else
                                numberOfPeopleWhoAlreadyOptedIn++;
                        }
                    }
                    if (numberOfNewOptIns > 0)
                        System.out.format ("\"%s\": Great! We raised opt-ins for this email address from %d to %d.%n", emailAddress, numberOfPeopleWhoAlreadyOptedIn, numberOfPeopleWhoAlreadyOptedIn + numberOfNewOptIns);
                    else if (numberOfPeopleWhoAlreadyOptedIn > 0)
                        System.out.format ("\"%s\": All %d people with this email address already opted into receiving emails.%n", emailAddress, numberOfPeopleWhoAlreadyOptedIn);
                    else
                        System.out.format ("\"%s\": Nobody has this email address.%n", emailAddress);
                }
                SerializeToXML.writeContacts(contacts);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private static void removePersonsFromContacts(String[] args) {
        for (int i=1; i <= args.length-1; i++)
        {
            String arg = args[i];

            contacts.removeIf (p -> p.getName().equals(arg));

        }
        SerializeToXML.writeContacts(contacts);
    }

    private static void addPersonToContacts(String[] args) {

        Person person;

        boolean person_exists=false;

        if (args.length==4) {


            person = new Person(args[1], LocalDate.parse(args[2], DateTimeFormatter.ISO_LOCAL_DATE), Person.Sex.valueOf(args[3].toUpperCase()), "");
        } else {
            person = new Person(args[1], LocalDate.parse(args[2], DateTimeFormatter.ISO_LOCAL_DATE), Person.Sex.valueOf(args[3].toUpperCase()), args[4].toLowerCase());
        }


            for (Person p : contacts) {
                if (p.getName().equalsIgnoreCase(person.getName()) || (p.getEmailAddress().equalsIgnoreCase(person.getEmailAddress()) && !person.getEmailAddress().equals(""))) {
                    System.out.println("Person 'name' " + person.getName() + " or 'eMail' " + person.getEmailAddress() + " already in use!");
                    person_exists=true;
                }
            }

            if (!person_exists) {


                contacts.add(person);
                SerializeToXML.writeContacts(contacts);
            }


    }

    private static void addPersonToNewList(String[] args) {
        contacts = new ArrayList<>();

        addPersonToContacts(args);
    }

    private static void readFile() throws FileNotFoundException {
        DeserializeFromXML.read();

        contacts = DeserializeFromXML.gerritsContacts.getContacts();
    }
    private static void readFilePrintDelete() throws FileNotFoundException {
        DeserializeFromXML.readPrintDelete();

        contacts = DeserializeFromXML.gerritsContacts.getContacts();
    }
    private static void readFileAdd() throws FileNotFoundException {
        DeserializeFromXML.readAdd();

        contacts = DeserializeFromXML.gerritsContacts.getContacts();
    }
    private static void printHelp() {
        System.out.println ("usage: java -jar calculatebirthdays [command] [args]");
        System.out.println("command: one of help, print, add, delete, opt-in or opt-out");
        System.out.println("add [args]: \"String name\" \"LocalDate birthday\" \"Sex gender\" \"String emailAddress\"");
        System.out.println("...birthday: yyyy-mm-dd");
        System.out.println("...gender: FEMALE, MALE or DIVERSE");
        System.out.println("delete [args]: \"String name\", \"...\"");
        System.out.println("opt-in [args]: \"String emailAddress\", \"...\"");
        System.out.println("opt-out [args]: \"String emailAddress\", \"...\"");
        System.out.println();


    }

    private static void printBirthdays(List<Person> contacts) {

        double result;

        System.out.println();
        System.out.println("*Boyz:*");
        System.out.println();
	System.out.println("Name (Age)            |Birthday |Gender   |eMail");
	System.out.println("----------------------|---------|---------|-----");


        printPersonsWithPredicate(
                contacts,
                //serializeToXML.contacts,
                p -> p.getGender() == Person.Sex.MALE
                        && p.getAge( LocalDate.now() ) >= 18
                        && p.getAge( LocalDate.now() ) < 60
        );

	System.out.println("----------------------|---------|---------|-----");

        System.out.println();
        System.out.println("*Girlz:*");
        System.out.println();
	System.out.println("Name (Age)            |Birthday |Gender   |eMail");
	System.out.println("----------------------|---------|---------|-----");

        printPersonsWithPredicate(
                contacts,
                //serializeToXML.contacts,
                p -> p.getGender() == Person.Sex.FEMALE
                        && p.getAge( LocalDate.now() ) >= 18
                        && p.getAge( LocalDate.now() ) < 60
        );

	System.out.println("----------------------|---------|---------|-----");

        System.out.println();
        System.out.println("*Diverse:*");
        System.out.println();
	System.out.println("Name (Age)            |Birthday |Gender   |eMail");
	System.out.println("----------------------|---------|---------|-----");


        printPersonsWithPredicate(
                contacts,
                //serializeToXML.contacts,
                p -> (p.getGender() == Person.Sex.DIVERSE  && p.getAge( LocalDate.now() ) >= 18
                        && p.getAge( LocalDate.now() ) < 60)

        );

	System.out.println("----------------------|---------|---------|-----");

        System.out.println();
        System.out.println("*Kidz:*");
        System.out.println();
	System.out.println("Name (Age)            |Birthday |Gender   |eMail");
	System.out.println("----------------------|---------|---------|-----");

        printPersonsWithPredicate(
                contacts,
                //serializeToXML.contacts,
                p -> (p.getGender() == Person.Sex.FEMALE || p.getGender() == Person.Sex.MALE || p.getGender() == Person.Sex.DIVERSE )
                        && p.getAge( LocalDate.now() ) < 18
        );

	System.out.println("----------------------|---------|---------|-----");

        System.out.println();
        System.out.println("*Seniors:*");
        System.out.println();
	System.out.println("Name (Age)            |Birthday |Gender   |eMail");
	System.out.println("----------------------|---------|---------|-----");

        printPersonsWithPredicate(
                contacts,
                //serializeToXML.contacts,
                p -> (p.getGender() == Person.Sex.FEMALE || p.getGender() == Person.Sex.MALE || p.getGender() == Person.Sex.DIVERSE)
                        && p.getAge( LocalDate.now() ) >= 60
        );

	System.out.println("----------------------|---------|---------|-----");

      /* Integer totalAge = deserializeFromXML.gerritsContacts.getContacts()
                .stream()
                .mapToInt(Person::getAge)
                .sum();

        System.out.println("Sum of ages (sum operation): " +
                totalAge);
*/


        System.out.println();
        System.out.println("*Next birthdays:* ");
        System.out.println();

	System.out.println("Name (Age)            |Birthday |Gender   |eMail");
	System.out.println("----------------------|---------|---------|-----");

        //sort(serializeToXML.contacts);

        sort(contacts);

	System.out.println("----------------------|---------|---------|-----");

        System.out.println();
        System.out.println("*Birthday paradoxon:* ");
        System.out.println();

        System.out.println("Number of Person in list: " + contacts.size());
        System.out.println();
        System.out.println("Collision of birthdays: ");  
        compareDayMonthSameDateAndPrint(contacts);
        System.out.println();

        result = (1.0d - probability_a_dash(contacts.size()));

        System.out.println("Probability P(A), A - two or more birthdays collide for n peoples, n=" + contacts.size() + " P(A)=" + result);
	System.out.println();

    }

    public static double probability_a_dash(int number_of_people) {

        double p = 1.0d;

        for (int i = 0; i<=(number_of_people-1); i++) {
	//	System.out.println("i: "+ i);		

            p *= (365-i)/365.0;
		
	//	System.out.println("p: "+ p);

        }

        return p;

    }

}
