package com.auticon.birthdays;

import java.beans.XMLDecoder;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class DeserializeFromXML {
    private static final String SERIALIZED_FILE_NAME="contacts.xml";
    private static final String SERIALIZED_FILE_NAME_1="contacts (1).xml";
    private static final String SERIALIZED_FILE_NAME_2="contacts (2).xml";

    public static Contacts gerritsContacts;

    public static void read() throws FileNotFoundException {
        XMLDecoder decoder=null;
        try {
            decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME)));
        } catch (FileNotFoundException e) {
            System.out.println("WARNING: File " + SERIALIZED_FILE_NAME + " not found");

            try {
                decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME_1)));
            } catch (FileNotFoundException e1) {
                System.out.println("WARNING: File " +  SERIALIZED_FILE_NAME_1 + " not found");
                try {
                    decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME_2)));
                } catch (FileNotFoundException e2) {
                    System.out.println("ERROR: File " + SERIALIZED_FILE_NAME_2 + " not found");
                    System.out.println();
                    System.out.println("use command add [args]: \"String name\" \"LocalDate birthday\" \"Sex gender\" \"String emailAddress\"");
			System.out.println("...birthday: yyyy-mm-dd");
			System.out.println("...gender: FEMALE, MALE or DIVERSE");
                    System.out.println();
                    throw new FileNotFoundException();


                }


            }



        }
        gerritsContacts = (Contacts) decoder.readObject();

    }

    public static void readPrintDelete() throws FileNotFoundException {
        XMLDecoder decoder=null;
        try {
            decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME)));
        } catch (FileNotFoundException e) {
 		System.out.println("WARNING: File " + SERIALIZED_FILE_NAME + " not found");

            try {
                decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME_1)));
            } catch (FileNotFoundException e1) {
		System.out.println("WARNING: File " +  SERIALIZED_FILE_NAME_1 + " not found");
                try {
                    decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME_2)));
                } catch (FileNotFoundException e2) {
  		    System.out.println("ERROR: File " + SERIALIZED_FILE_NAME_2 + " not found");
                    System.out.println();
                      System.out.println("use command add [args]: \"String name\" \"LocalDate birthday\" \"Sex gender\" \"String emailAddress\"");
			System.out.println("...birthday: yyyy-mm-dd");
			System.out.println("...gender: FEMALE, MALE or DIVERSE");
                    System.out.println();
                    throw new FileNotFoundException();


                }


            }



        }
        gerritsContacts = (Contacts) decoder.readObject();
        System.out.println(gerritsContacts);

    }

    public static void readAdd() throws FileNotFoundException {
        XMLDecoder decoder=null;
        try {
            decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME)));
        } catch (FileNotFoundException e) {
		System.out.println("WARNING: File " + SERIALIZED_FILE_NAME + " not found");

            try {
                decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME_1)));
            } catch (FileNotFoundException e1) {
 		System.out.println("WARNING: File " +  SERIALIZED_FILE_NAME_1 + " not found");
                try {
                    decoder=new XMLDecoder(new BufferedInputStream(new FileInputStream(SERIALIZED_FILE_NAME_2)));
                } catch (FileNotFoundException e2) {
                       System.out.println("WARNING: File " + SERIALIZED_FILE_NAME_2 + " not found");
                    throw new FileNotFoundException();


                }


            }



        }
        gerritsContacts = (Contacts) decoder.readObject();
        System.out.println(gerritsContacts);

    }
}
