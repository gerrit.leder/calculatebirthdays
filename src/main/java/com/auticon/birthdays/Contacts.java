package com.auticon.birthdays;

import java.util.ArrayList;
import java.util.List;

public class Contacts {
    private List<Person> contacts =new ArrayList();
    public Contacts(){}
    public Contacts(List<Person> personsList) {setContacts(personsList);}
    public List<Person> getContacts() {
        return contacts;
    }
    public void setContacts(List<Person> personsList) {
        this.contacts = personsList;
    }
    public String toString(){
        String contacts="";
        for(Person person : getContacts()){
            contacts += person.getName()+", ";
        }
        return contacts;
    }
}
